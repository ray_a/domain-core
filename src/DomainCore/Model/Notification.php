<?php
/**
 * ドメイン通知
 */
namespace DomainCore\Model;

class Notification
    extends ValueObjectAbstract
    implements IValueObject
{

    /** 通知種別 @var string */
    private $type;

    /** 通知メッセージ @var string */
    private $message;

    public static function __callStatic ($name, array $arguments)
    {
        $message = reset($arguments);
        return static::instance($name, $message);
    }

    /**
     * constructor
     * @param string $type 通知種別
     * @param string $message 通知メッセージ
     * @return void
     */
    public function __construct ($type, $message)
    {
        $this->type = strtolower($type);
        $this->message = $message;
    }

    /**
     * 特定の通知種別か
     * @param string $type 通知種別
     * @return boolean
     */
    public function isType ($type)
    {
        return strtolower($type) == $this->type;
    }

}
