<?php
/**
 * 抽象仕様
 */
namespace DomainCore\Model;

abstract class SpecAbstract
    extends DomainServiceAbstract
{

    /** エラーハンドラ @var INotificationHandler */
    protected $notificationHandler;

    public function __construct (INotificationHandler $notificationHandler = null)
    {
        $this->notificationHandler = is_null($notificationHandler)
            ? new DoNothingNotificationHandler()
            : $notificationHandler;
    }

    /**
     * ドメイン通知ハンドラを設定する
     * @param INotificationHandler $notificationHandler ドメイン通知ハンドラ
     * @return self
     */
    public function setHandler (INotificationHandler $notificationHandler)
    {
        $this->notificationHandler = $notificationHandler;
        return $this;
    }

    /**
     * 全てが真か
     * @param ...mixed ...$expression 検証対象リスト
     * @return boolean
     */
    protected function all (...$expressions)
    {
        return !in_array(false, $expressions);
    }

    /**
     * 通知する
     * @param string $category カテゴリ
     * @param string $type ドメイン通知種別
     * @param string $message 通知メッセージ
     * @return self
     */
    protected function notify ($category, $type, $message)
    {
        $this->notificationHandler->notify($category, Notification::{$type}($message));
        return $this;
    }

}
