<?php
/**
 * 何もしないドメイン通知ハンドラ
 */
namespace DomainCore\Model;

final class DoNothingNotificationHandler
    extends DomainServiceAbstract
    implements INotificationHandler
{

    /**
     * @inheritdoc
     */
    public function categories ()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function hasCategory ($category)
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public function notify ($category, Notification $notification)
    {
        return;
    }

    /**
     * @inheritdoc
     */
    public function hasType ($type, $category = null)
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public function notifications ($category, array $filter = [])
    {
        return [];
    }

}
