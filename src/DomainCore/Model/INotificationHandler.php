<?php
/**
 * ドメイン通知インターフェース
 */
namespace DomainCore\Model;

interface INotificationHandler
{

    /**
     * カテゴリリスト
     * @return string[]
     */
    function categories ();

    /**
     * カテゴリを持つか
     * @return boolean
     */
    function hasCategory ($category);

    /**
     * 通知する
     * @param string $category 通知カテゴリ
     * @param Notification $notification ドメイン通知
     * @return self
     */
    function notify ($category, Notification $notification);

    /**
     * 特定のドメイン通知種別を持つか
     * @param string $type ドメイン通知種別
     * @param string $category OPTIONAL 通知カテゴリ
     * @return boolean
     */
    function hasType ($type, $category = null);

    /**
     * ドメイン通知リスト
     * @param string[] $filter ドメイン通知種別フィルタ
     * @return Notification[]
     */
    function notifications ($category, array $filter = []);

}
