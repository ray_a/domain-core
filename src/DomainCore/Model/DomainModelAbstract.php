<?php
/**
 * 抽象ドメインモデル
 */
namespace DomainCore\Model;

use PhpTypeExtension\Traits;

abstract class DomainModelAbstract
{

    /** アクセス禁止フィールドプレフィクスリスト @var string[] */
    private $inaccessiblePrefixList = [];

    /* 読み取り専用プロパティトレイト */
    use Traits\ReadOnlyFieldsDefinitionable {
        Traits\ReadOnlyFieldsDefinitionable::__get as traitGet;
    }

    /* 静的ファクトリトレイト */
    use Traits\StaticInstantiatable;

    /**
     * constructor
     * @param string[] $inaccessiblePrefixList アクセス禁止フィールドプレフィクスリスト
     * @return void
     */
    protected function __construct (array $inaccessiblePrefixList = [])
    {
        $this->inaccessiblePrefixList = $inaccessiblePrefixList;
    }

    /**
     * プロパティアクセスによるゲッター呼び出しのサポート
     * 読み取り専用プロパティアクセスのサポート
     * @param string $name プロパティ名
     * @return mixed
     */
    public function __get ($name)
    {
        $pattern = implode('|', $this->inaccessiblePrefixList);
        if ($this->isInaccessible($name)) {
            throw new \RuntimeException("Undefined property via __get(): {$name}");
        }
        return \Closure::bind(
            function ($name) {
                $getter = 'get' . ucfirst($name);
                if (method_exists($this, $getter)) {
                    return $this->{$getter}();
                }
                return $this->traitGet($name);
            }, $this, get_class($this)
        )->__invoke($name);
    }

    /**
     * アクセス禁止フィールドか
     * @param string $name
     * @return boolean
     */
    protected function isInaccessible ($name)
    {
        $pattern = implode('|', $this->inaccessiblePrefixList);
        return !empty($pattern) && preg_match("/^{$pattern}/u", $name) == 1;
    }

    /**
     * アクセス禁止フィールドプレフィクスを追加する
     * @param ...string ...$prefixList プレフィクスリスト
     * @return self
     */
    protected function addInaccessiblePrefix (...$prefixList)
    {
        foreach ($prefixList as $prefix) {
            $this->inaccessiblePrefixList[] = $prefix;
        }
        return $this;
    }

}
