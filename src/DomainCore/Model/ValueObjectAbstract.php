<?php
/**
 * 抽象値オブジェクト
 */
namespace DomainCore\Model;

abstract class ValueObjectAbstract
    extends DomainModelAbstract
{

    /**
     * 未定義メソッド呼び出しサポート
     * @param string $name メソッド名
     * @param mixed[] $args OPTIONAL メソッド引数リスト
     * @return mixed
     */
    public function __call ($name, array $args = [])
    {
        if (preg_match('/^with(.+)$/u', $name, $matches)) {
            return $this->with(lcfirst($matches[1]), reset($args));
        }
        $className = static::class;
        throw new \BadMethodCallException(
            "call to undifined method {$className}::{$name} via __call");
    }

    /**
     * 任意のプロパティ値を設定した新しいインスタンスを生成するwithメソッドのサポート
     * @param string $name プロパティ名
     * @param mixed $value プロパティ値
     * @return self
     */
    private function with ($name, $value)
    {
        return \Closure::bind(
            function ($name, $value) {
                if (!$this->isInaccessible($name)
                    && in_array($name, array_keys(get_object_vars($this)))
                ) {
                    $instance = clone $this;
                    $instance->{$name} = $value;
                    return $instance;
                }
                $className = static::class;
                $method = 'with' . ucfirst($name);
                throw new \BadMethodCallException(
                    "{$className}::{$method}に対応するフィールド{$name}が未定義です");
            }, $this, get_class($this)
        )->__invoke($name, $value);
    }

}
