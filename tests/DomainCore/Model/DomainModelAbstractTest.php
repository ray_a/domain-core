<?php
/**
 * 抽象ドメインモデル ユニットテスト
 */
namespace Tests\DomainCore\Model;

use Tests\DomainCore\Model\DomainModelAbstract\TestMember;

/**
 * @coversDefaultClass DomainCore\Model\DomainModelAbstract
 */
final class DomainModelAbstractTest
    extends \Tests\TestAbstract
{

    /**
     * @covers ::__construct
     * @covers ::__get
     * @covers ::isInaccessible
     */
    public function test_get ()
    {
        /* Arrange */
        $id = 1;
        $lastName = 'testLastName';
        $firstName = 'testFirstName';
        $member = TestMember::instance($id, $lastName, $firstName);
        /* Act */
        /* Assert */
        $this->assertEquals($id, $member->id);
        $this->assertEquals($lastName, $member->lastName);
        $this->assertEquals($firstName, $member->firstName);
        $this->assertEquals("{$lastName} {$firstName}", $member->fullName);
    }

    /**
     * @covers ::__construct
     * @covers ::__get
     * @covers ::isInaccessible
     * @expectedException \RuntimeException
     * @expectedExceptionMessageRegExp /^Undefined property via __get\(\): _version$/
     */
    public function test_getOnInAccessibleFiledThrowsException ()
    {
        /* Arrange */
        $id = 1;
        $lastName = 'testLastName';
        $firstName = 'testFirstName';
        $member = TestMember::instance($id, $lastName, $firstName);
        /* Act */
        $version = $member->_version;
        /* Assert */
    }

    /**
     * @covers ::__construct
     * @covers ::__get
     * @covers ::isInaccessible
     * @expectedException \RuntimeException
     * @expectedExceptionMessageRegExp /^Undefined property via __get\(\): birthday$/
     */
    public function test_getOnUndifinedFieldThrowsException ()
    {
        /* Arrange */
        $id = 1;
        $lastName = 'testLastName';
        $firstName = 'testFirstName';
        $member = TestMember::instance($id, $lastName, $firstName);
        /* Act */
        $birthday = $member->birthday;
        /* Assert */
    }

    /**
     * @group lab
     * test
     * 実際には`traitGet`を直接呼ぶことで、`inaccessiblePrefix`をもつ
     * フィールドにアクセス可能
     */
    public function traitGetIsPublicCallable ()
    {
        /* Arrange */
        $id = 1;
        $lastName = 'testLastName';
        $firstName = 'testFirstName';
        $member = TestMember::instance($id, $lastName, $firstName);
        /* Act */
        $version = $member->traitGet('_version');
        echo "\n"; var_dump($version);
    }

}

namespace Tests\DomainCore\Model\DomainModelAbstract;

use DomainCore\Model;

class TestMember
    extends Model\DomainModelAbstract
{

    private $id;
    private $lastName;
    private $firstName;
    private $_version;

    public function __construct ($id, $lastName, $firstName, $version = null)
    {
        parent::__construct(['_']);
        $this->id = $id;
        $this->lastName = $lastName;
        $this->firstName = $firstName;
        $this->_version = is_null($version) ? 1 : $version;
    }

    public function getFullName ()
    {
        return "{$this->lastName} {$this->firstName}";
    }

}
