<?php
/**
 * ドメイン通知 ユニットテスト
 */
namespace Tests\DomainCore\Model;

use DomainCore\Model as DomainCore;

/**
 * @coversDefaultClass DomainCore\Model\Notification
 */
final class NotificationTest
    extends \Tests\TestAbstract
{

    /**
     * @group isType
     * @covers ::__construct
     * @covers ::isType
     */
    public function test_isType ()
    {
        /* Arrange */
        $type = 'error';
        $message = 'test error message';
        /* Act */
        $notification = DomainCore\Notification::instance($type, $message);
        /* Assert */
        $this->assertTrue($notification->isType('error'));
        $this->assertFalse($notification->isType('warning'));
    }

    /**
     * @group __callStatic
     */
    public function test_callStatic ()
    {
        /* Arrange */
        $type = 'eRrOr';
        $message = 'error message';
        /* Act */
        $notification = DomainCore\Notification::{$type}($message);
        /* Assert */
        $this->assertEquals(strtolower($type), $notification->type);
        $this->assertTrue($notification->isType($type));
        $this->assertEquals($message, $notification->message);
    }

}
