<?php
/**
 * 抽象仕様 ユニットテスト
 */
namespace Tests\DomainCore\Model;

use DomainCore\Model as DomainCore;
use Tests\DomainCore\Model\SpecAbstract\TestSpec;

/**
 * @coversDefaultClass DomainCore\Model\SpecAbstract
 */
final class SpecAbstractTest
    extends \Tests\TestAbstract
{

    /**
     * @group setHandler
     * @covers ::setHandler
     * @covers ::__construct
     */
    public function test_setHandler ()
    {
        /* Arrange */
        $handlerProphecy = $this->prophesize(DomainCore\INotificationHandler::class);
        /* Act */
        $spec = TestSpec::instance()->setHandler($handlerProphecy->reveal());
        /* Assert */
        \Closure::bind(
            function ($test) use ($handlerProphecy) {
                $test->assertEquals($handlerProphecy->reveal(), $this->notificationHandler);
            }, $spec, get_class($spec)
        )->__invoke($this);
    }

    /**
     * @group all
     * @covers ::all
     */
    public function test_all ()
    {
        /* Arrange */
        $spec = TestSpec::instance();
        /* Act */
        /* Assert */
        \Closure::bind(
            function ($test) {
                $test->assertTrue($this->all(true, true, true));
                $test->assertFalse($this->all(true, false, true));
            }, $spec, get_class($spec)
        )->__invoke($this);

    }

    /**
     * @group notify
     * @covers ::notify
     * @covers ::__construct
     */
    public function test_notify ()
    {
        /* Arrange */
        $handlerProphecy = $this->prophesize(DomainCore\INotificationHandler::class);
        $spec = TestSpec::instance($handlerProphecy->reveal());
        $notificationList = [
            ['category' => 'category1', 'type' => 'error', 'message' => 'error1-1'],
            ['category' => 'category1', 'type' => 'error', 'message' => 'error1-2'],
            ['category' => 'category2', 'type' => 'warning', 'message' => 'warning2-1'],
            ['category' => 'category2', 'type' => 'error', 'message' => 'error2-1'],
            ['category' => 'category3', 'type' => 'notice', 'message' => 'notice3-1'],
        ];
        /* Act */
        \Closure::bind(
            function ($notificationList) {
                foreach ($notificationList as $notification) {
                    $this->notify(
                        $notification['category'], $notification['type'],
                        $notification['message']);
                }
            }, $spec, get_class($spec)
        )->__invoke($notificationList);
        /* Assert */
        foreach ($notificationList as $notification) {
            list($category, $type, $message) = array_values($notification);
            $result = $handlerProphecy
                ->notify($category, DomainCore\Notification::{$type}($message))
                ->shouldHaveBeenCalled();
        }
    }

}

namespace Tests\DomainCore\Model\SpecAbstract;

use DomainCore\Model;

class TestSpec
    extends Model\SpecAbstract
{
}
