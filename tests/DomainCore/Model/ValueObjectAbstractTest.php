<?php
/**
 * 抽象値オブジェクト ユニットテスト
 */
namespace Tests\DomainCore\Model;

use Tests\DomainCore\Model\ValueObjectAbstract\TestName;

/**
 * @coversDefaultClass DomainCore\Model\ValueObjectAbstract
 */
final class ValueObjectAbstractTest
    extends \Tests\TestAbstract
{

    /**
     * @covers ::__call
     * @covers ::with
     */
    public function test_with ()
    {
        /* Arrange */
        $firstName = 'testFirstName';
        $lastName = 'testLastName';
        $newLastName = 'testNewLastName';
        $name = TestName::instance($lastName, $firstName);
        /* Act */
        $newName = $name->withLastName($newLastName);
        /* Assert */
        $this->assertEquals($firstName, $name->firstName);
        $this->assertEquals($lastName, $name->lastName);
        $this->assertEquals($firstName, $newName->firstName);
        $this->assertEquals($newLastName, $newName->lastName);
    }

    /**
     * @covers ::with
     * @expectedException \BadMethodCallException
     * @expectedExceptionMessageRegExp /^.+::withBirthdayに対応するフィールドbirthdayが未定義です$/
     */
    public function test_withOnUndifinedFieldThrowsException ()
    {
        /* Arrange */
        $firstName = 'testFirstName';
        $lastName = 'testLastName';
        $newLastName = 'testNewLastName';
        $name = TestName::instance($lastName, $firstName);
        /* Act */
        $newName = $name->withBirthday('1975-08-04');
        /* Assert */
    }

    /**
     * @covers ::__call
     * @expectedException \BadMethodCallException
     * @expectedExceptionMessageRegExp /^call to undifined method .+::getFullName via __call$/
     */
    public function test_callUndifinedMethodThrowsException ()
    {
        /* Arrange */
        $firstName = 'testFirstName';
        $lastName = 'testLastName';
        $newLastName = 'testNewLastName';
        $name = TestName::instance($lastName, $firstName);
        /* Act */
        $fullName = $name->getFullName();
        /* Assert */
    }

    /**
     * @covers ::with
     * @expectedException \BadMethodCallException
     * @expectedExceptionMessageRegExp /^.+::with_versionに対応するフィールド_versionが未定義です$/
     */
    public function test_withOnInAccessibleField ()
    {
        /* Arrange */
        $version = 12;
        $lastName = 'testLastName';
        $firstName = 'testFirstName';
        $name = TestName::instance($lastName, $firstName, $version);
        /* Act */
        $newName = $name->with_version($version + 1);
        /* Assert */
        \Closure::bind(
            function () {
                echo "\n"; var_dump($this->_version);
            }, $newName, get_class($newName)
        )->__invoke();
    }

}

namespace Tests\DomainCore\Model\ValueObjectAbstract;

use DomainCore\Model;

class TestName
    extends Model\ValueObjectAbstract
{

    private $_version;
    private $firstName;
    private $lastName;

    public function __construct ($lastName, $firstName, $version = null)
    {
        parent::__construct(['_']);
        $this->_version = is_null($version) ? 1 : $version;
        $this->lastName = $lastName;
        $this->firstName = $firstName;
    }

}
