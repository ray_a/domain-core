<?php
/**
 * 何もしないドメイン通知ハンドラ ユニットテスト
 */
namespace Tests\DomainCore\Model;

use DomainCore\Model as DomainCore;

/**
 * @coversDefaultClass \DomainCore\Model\DoNothingNotificationHandler
 */
final class DoNothingNotificationHandlerTest
    extends \Tests\TestAbstract
{

    /**
     * @covers ::categories
     * @covers ::hasCategory
     * @covers ::notify
     * @covers ::hasType
     * @covers ::notifications
     */
    public function test_behavior ()
    {
        /* Arrange */
        $handler = $this->model();
        $category = 'testCategory';
        $type = 'testType';
        $message = 'test message';
        $notification = DomainCore\Notification::{$type}($message);
        /* Act */
        $handler->notify($category, $notification);
        /* Assert */
        $this->assertEmpty($handler->categories());
        $this->assertFalse($handler->hasCategory($category));
        $this->assertFalse($handler->hasType($type));
        $this->assertEmpty($handler->notifications($category));
    }

    private function model ()
    {
        return DomainCore\DoNothingNotificationHandler::instance();
    }

}
